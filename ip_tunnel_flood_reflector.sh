#!/bin/bash

crpd=cg_fti
host=$(hostname)

function gre {
local=$1
remote=$2
address=$3
interface=$4

ip link del $interface 
ip tunnel add $interface  mode gre local $local remote $remote ttl 255
ip addr add $address dev $interface
ip link set $interface up mtu 1600 

}

#modprobe ip_gre

case $host in 
${crpd}3)
echo "setting up instance 3"

ip link delete gre34
ip link delete gre35
ip link delete gre36
gre 10.3.9.1 10.3.9.2 192.168.39.1/30 fti0.39
cli -c "configure;load override /config/cg_fr3.cfg;commit and-quit" 
;;

${crpd}4)
echo "setting up instance 4"
ip link delete gre43
ip link delete gre45
ip link delete gre46
gre 10.4.9.1 10.4.9.2 192.168.49.1/30 fti0.49
cli -c "configure;load override /config/cg_fr4.cfg;commit and-quit" 
;;

${crpd}5)
echo "setting up instance 5"
ip link delete gre53
ip link delete gre54
ip link delete gre56
gre 10.5.9.1 10.5.9.2 192.168.59.1/30 fti0.59
cli -c "configure;load override /config/cg_fr5.cfg;commit and-quit" 
;;

${crpd}6)
echo "setting up instance 6"
ip link delete gre63
ip link delete gre64
ip link delete gre65
gre 10.6.9.1 10.6.9.2 192.168.69.1/30 fti0.69
cli -c "configure;load override /config/cg_fr6.cfg;commit and-quit" 
;;

${crpd}9)
echo "setting up instance 9"
gre 10.3.9.2 10.3.9.1 192.168.39.2/30 fti0.93
gre 10.4.9.2 10.4.9.1 192.168.49.2/30 fti0.94
gre 10.5.9.2 10.5.9.1 192.168.59.2/30 fti0.95
gre 10.6.9.2 10.6.9.1 192.168.69.2/30 fti0.96
cli -c "configure;load override /config/cg_fr9.cfg;commit and-quit" 
;;

*)
echo "fail"
;;
esac

ip a show | grep -E "fti0.[3-9]"

