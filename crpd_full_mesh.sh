#!/bin/bash

# make your changes here
########################
instance=2                              # in case you are running multiple setup, 
                                        # each should have its own unique instance id
                                        # id is numeric and the used OOB gets derieved from it
                                        # script allows masquearading to e.g. ssh from ext into crpd
                                        # the masqueraded port gets derived as well from ID
prefix_crpd=cg_fti   		        # prefix_name of the cRPD container
oob_bridge=${prefix_crpd}               # keep equal to prefix_crpd
oob_cidr=172.16.$instance               # Format is without the /24 cidr, e.g  172.16.50 
masq_ip=176.9.138.74                    # set to empty string "" or to the ip-address to masquerade
#masq_ip=""                             # set to empty string "" or to the ip-address to masquerade
base_port=$((9900+200*instance))        # when masquerade then crpd is base_port + id

prefix_link="link"                      # prefix of the links to create
#prefix_link=${prefix_crpd}             # prefix of the links to create
                                        # must be shorter or equa then 15chars in total
                                        # the script adds 2 * underscores and 2 * number
                                        # with e.g. 15 instances the longest name would be
                                        # cgv_15_14
                                        # so in most cases add 6 chars on top of prefix 
crpd_image="crpd:20.4R1.12"		# name of crpd docker image. Format in Repository:Tag
					# no need to have it equal to the container-name
instances=11                             # number of crpd instances to create

# do not change below
#####################
link_sub="10"                           # its a lab. easy to remember numbering is important
                                        # hence the used numbering is wasting 10/8
                                        # link node 30 to node 41 uses this subnet: 10.30.41/24

function check_root {
# check if root
if [ "$EUID" -ne 0 ]
then 
	echo "Please run as root"
	echo "exiting now"
        exit
fi
}

function mpls {
modprobe mpls_iptunnel >  /dev/null 2>&1 
modprobe mpls_router >  /dev/null 2>&1 
modprobe ip_tunnel >  /dev/null 2>&1 

fail=0
lsmod|grep mpls_iptunnel >  /dev/null 2>&1 || fail=true
lsmod|grep mpls_router >  /dev/null 2>&1 || fail=true

if [[ $fail == "true" ]] 
then
	echo "loading of either module mpls_iptunnel or module mpls_router failed"
	echo "mpls forwarding will be broken"
	echo "please try loading the modules via:"
	echo "modprobe mpls_iptunnel"
	echo "modprobe mpls_router"
	echo "press enter to continue - but have in mind that mpls forwarding is not functional"
	read key

fi
}


function check_add_phy {
file=add_phy.sh

if [[ -x "$file" ]]
then
	    #echo "File '$file' is executable"
	    echo .
    else
	        echo .
		echo "##### fatal error ######"
		echo "File '$file' is not executable or found"
		echo "have you made the $file executable via 'chmod +x $file' command?"
		echo "now exiting"
		exit 0
fi

}

function stop_crpd {
echo "now stopping crpd instances starting with prefix ${prefix_crpd}"
echo "give it some time"
while IFS= read -r line
do
   docker stop $line&
done < <(docker ps --filter "name=${prefix_crpd}*" --format "{{.Names}}")

# wailt until all containers down
 crpd=${prefix_crpd}
  # check if container is running. if yes, bring down and remove
  while  docker ps | grep $crpd >  /dev/null 2>&1 
  do
    sleep 1
  done 
echo "all ${prefix_crpd}* instances have been stopped successfully"

}


function check_volume {
config_volume=$1
  # check if config volume exists. if not, then create it
  if docker volume ls | grep ${config_volume} >  /dev/null 2>&1
  #if $(( docker volume inspect ${config_volume} ))
  then
    echo "${config_volume} existing. good"
  else
        echo "$config_volume not existing. creating it"
        docker volume create ${config_volume}
  fi
}

function add_delete_veth {
# creating or delete the veth links
for ((x = 1 ; x <= ${instances}; x++))
do
  start=$((x+1))
  for ((y=start ; y<=${instances};y++))
  do
        link_yx=${prefix_link}_${y}_${x}
        link_xy=${prefix_link}_${x}_${y}
        crpd_x=${prefix_crpd}${x}
        crpd_y=${prefix_crpd}${y}
        # interfaces shall be created
        if [ $1 == "create" ]
        then
            # check if veth exists already. 
            if ip link show type veth  ${link_xy} > /dev/null 2>&1
            then
                echo " ${link_xy}  existing. good. not creating new"
            else
                # interface does not yet exist. creating it
                echo "creating veth  ${link_xy} and its peer  ${link_yx}"
                ip link add  ${link_xy} type veth  peer name  ${link_yx} 
            fi

        else
            # interfaces shall be deleted
            ip link show type veth  ${link_xy} > /dev/null 2>&1 && ip link delete ${link_xy}
        fi      
  done
done
}

function cleanup_all_veth {
echo "deleting any leftover veth interface with prefix ${prefix_link} in default instance"
while IFS= read -r line
do
   ip link delete $line > /dev/null 2>&1
done < <(ip link show type veth|grep ${prefix_link} |cut -d" " -f2|cut -d"@" -f1)

echo "done deleting veth interfaces ${prefix_link} in default instance"
}

function move_interfaces {
#out=debug.txt
#touch $out
#cp /dev/null $out

for ((x = 1 ; x <= ${instances}; x++))
do
  start=$((x+1))
  for ((y=start ; y<=${instances};y++))
  do
        link_yx=${prefix_link}_${y}_${x}
        link_xy=${prefix_link}_${x}_${y}
        crpd_x=${prefix_crpd}${x}
        crpd_y=${prefix_crpd}${y}


            echo "calling add_phy.sh to move interface ${link_yx}  to container ${crpd_y} "
            ./add_phy.sh ${crpd_y}  ${link_yx}   
            echo "calling add_phy.sh to move interface ${link_xy}  to container ${crpd_x} "
            ./add_phy.sh ${crpd_x}  ${link_xy} 

#	    echo "${crpd_y}  ${link_yx}" >> $out
#	    echo "${crpd_x}  ${link_xy}" >> $out

done
done
}


function assign_addresses {
echo "starting function assign_addresses"
for ((x = 1 ; x <= ${instances}; x++))
do
  start=$((x+1))
  for ((y=start ; y<=${instances};y++))
  do
        link_yx=${prefix_link}_${y}_${x}
        link_xy=${prefix_link}_${x}_${y}
        crpd_x=${prefix_crpd}${x}
        crpd_y=${prefix_crpd}${y}
        # set ip address of the links   
        # we need to ensure interface arrived at the namespace 
        while ! (ip netns exec ${crpd_x} ip link show ${link_xy})
        do
        sleep 1
        done
        while ! (ip netns exec ${crpd_y} ip link show ${link_yx})
        do
        sleep 1
        done

    
        ip netns exec ${crpd_x} ip address add "${link_sub}.$x.$y.1/24" dev ${link_xy}
        ip netns exec ${crpd_y} ip address add "${link_sub}.$x.$y.2/24" dev ${link_yx}
        ip netns exec ${crpd_x} ip link set dev ${link_xy} up
        ip netns exec ${crpd_y} ip link set dev ${link_yx} up
done
    # setting the loopbacks
            ip netns exec ${prefix_crpd}$x ip address add "172.16.53.$x" dev lo
            ip netns exec ${prefix_crpd}$x ip -6 address add "::ffff:172.16.53.$x" dev lo
done
}

function launch_oob {
#docker network create ${oob_bridge}
# in any case kill the bridge
if docker network ls --format {{.Name}} |grep ${oob_bridge} 
then
    # docker network exitss. we need to stop it
    docker network rm ${oob_bridge}
    # making sure its removed
    while docker network inspect ${oob_bridge}
    do
        sleep 1
    done
fi

# checking if pool is in use by another network
while IFS= read -r line
do
   if docker network inspect $line |grep Subnet |grep ${oob_cidr}
    then
        echo " ###### error.. ########"
        echo "configured oob_cidr $oob_cidr is already in use by bridge $line"
        echo "either shutdown bridge $line via command : docker network rm ${line}"
        echo "or confgure a different oob_cidr=$oob_cidr in the variable section of this script"
        echo "exiting now"
        exit 0
    fi
done < <(docker network ls --format "{{.Name}}")


if [ $1 == "create" ]
then
  # creating it freshly
  docker network create \
    --driver=bridge \
    --subnet=${oob_cidr}.0/24 \
    --ip-range=${oob_cidr}.0/24 \
    --gateway=${oob_cidr}.254 \
    ${oob_bridge}

  # making sure the bridge is up before cont
  while ! docker network inspect ${oob_bridge}
  do
    sleep 1
  done
fi
}

function check_docker_image {
if docker image ls --format "table {{.Repository}}:{{.Tag}}"|grep ${crpd_image}
then
	echo ${crpd_image} existing. good
else
	echo .
	echo "###### fatal error #######"
	echo "the configured image in variable crpd_image=${crpd_image} is missing"
	echo "either change the variable ${crpd_image} with Format Repository:Tag to match your existing crpd docker image"
	echo "or get ${crpd_image} loaded "
	echo "exiting now"
	exit
fi
}

function start_crpd {
# looping through all the instances
for ((i = 1 ; i <= $instances ; i++))
do
# setting vars
crpd=${prefix_crpd}$i
config_volume=${crpd}_config


  # check if config volume exists. if not, then create it
  check_volume ${config_volume}

  # check if the configured docker image is available
 check_docker_image 

 # starting the containers
if [ -z ${masq_ip} ]
then
   # empty string 
   # means we do not want to expose oob-interfaces to external via masq
   docker run --rm --detach --name $crpd -h $crpd --privileged --net=${oob_bridge} --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${config_volume}:/config -it ${crpd_image} &
   #docker run --rm --detach --name $crpd -h $crpd --privileged --net=none --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${config_volume}:/config -it ${crpd_image} &
else
  # enable docker Masquerade to expose ssh-ports for crpd
  ext=$((base_port+i))
    
  # verify if host-port is free or occupied
  if netstat -na|grep ":$ext"
    then
        echo "exposing port 22 of crpd instance $crpd via port $ext is not possible"
        echo "please use other base_port in configured variables of $base_port"
        echo "or shutdown the affecting crpd instance"
        echo "exiting now"
        exit 0
  fi
  docker run --rm --detach --name $crpd -h $crpd --privileged --net=${oob_bridge} -p ${masq_ip}:$ext:22 --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${config_volume}:/config -it ${crpd_image} &
fi
done

}

function verify {

echo "check veth interfaces in main-instance"
echo "output shall be empty, as all veth sall be assigned to crpd"
echo "press enter to cont"
ip link show type veth | grep ${prefix_link}
read ee

echo "check crpd instances"
docker ps -a |grep ${prefix_crpd}
echo "press enter to cont"
read ee


# checking if veth interfaces are assigned to crpd instances
for ((i = 1 ; i <= $instances ; i++))
do
# setting vars
crpd=${prefix_crpd}$i
echo "============== checking ip link inside $crpd namespace =============="
ip netns exec $crpd ip address show |grep "lo\|${prefix_link}"

count=$(ip netns exec $crpd ip link show type veth |grep ${prefix_link}|wc -l)
echo "amount of veth:" $count
echo .
echo "press key to cont"
read ee
done
}

function crpdclean
{
echo "allows to stop left-over crpd instances"
echo "enter crpd name prefix, e.g. cg_fm"
read x
echo "the following crpd instances are going to be shutdown now:"
docker ps --filter "name=${x}.*" --format "{{.Names}}"
echo "are you sure to continue. Enter y to continue and shutting down instances"
read confirm
case $confirm in 
[yY]) 
        echo "starting shuwdown of crpd instances $x"
        while IFS= read -r line
            do
            docker stop $line&
        done < <(docker ps --filter "name=${x}.*" --format "{{.Names}}")
;;
*)
        echo "not shutting down anything"

;;
esac
}

function hello {
    echo .
    echo "##########################################"
    echo "############ setup finished ##############"
    echo "##########################################"
    echo .
    echo "Press enter to print summary"
    read x

    echo "containers up and running"
for ((i = 1 ; i <= $instances ; i++))
do
crpd=${prefix_crpd}$i
oob=${oob_cidr}.$i
port=$((base_port+i))
echo "crpd_instance $crpd uses oob $oob and externally exposed via ip:port ${masq_ip}:$port"
done

echo .
echo "Press enter to continue printing the summary of the script"
read x
echo .
    
    cat << EXAMPLES1
examples to connect to first running instance
========
e.g. logging into first crpd instance
docker shell using "docker exec"
    docker exec -ti ${prefix_crpd}1 cli
    docker exec -ti ${prefix_crpd}1 bash
    
enabling ssh
    you might want to enable ssh following the manual: 
    https://www.juniper.net/documentation/en_US/crpd/topics/topic-map/establishing-ssh-crpd.html
    
ssh from local host into first crpd instance
    ssh -l root ${oob_cidr}.1

ssh from a remote host into first crpd instance
(works only if variable masq_ip is set to your externally reachable interface)
    ssh -p $((base_port+1)) -l root $masq_ip

EXAMPLES1

echo .
echo "Press enter to continue printing the summary of the script"
read x
echo .

cat << EXAMPLES2
link naming and addressing
=========================
configured links
    all instances are fully meshed configured
    e.g. on the 3rd instance ${prefix_crpd}3 the link named to connect to instance 14 is: ${prefix_crpd}_3_14
    e.g. on the 14th instance ${prefix_crpd}14 the link named to connect to instance 3 is: ${prefix_crpd}_14_3

ip-addresses on the links
    instance ${prefix_crpd}3 link ${prefix_crpd}_3_14 has ip ${link_sub}.3.14.1/24
    instance ${prefix_crpd}14 link ${prefix_crpd}_14_3 has ip ${link_sub}.3.14.2/24


have fun
EXAMPLES2
}

###############
# main prg
###############
# check if running with sudo-root
check_root

# check if add_phy.sh is executable
check_add_phy

# check provided command parms
case $1 in

stop)
    stop_crpd
    add_delete_veth delete
    cleanup_all_veth
    launch_oob stop
    ;;

start)
    mpls
    stop_crpd
    add_delete_veth delete
    # launch the oob bridge
    launch_oob create
    start_crpd
    add_delete_veth create
    move_interfaces
    assign_addresses
    cleanup_all_veth
    hello
    ;;
verify)
    verify
    ;;
vethclean)
    cleanup_all_veth
    ;;

crpdclean)
    crpdclean
    ;;

*)
    echo "usage $0 start|stop|verify|vethclean|crpdclean"
    cat << USAGE
       .
       start   : launching topology.
                in detail that means:
                 - if containers with same prefix exist, stop them
                 - if not existing, create veth interfaces
                 - start crpd instances
                 - move interfaces into crpd namespaces
                 - assign lo and link addresses
                 - if from older tests veth interfaces with configured prefix exist, delete those from defaut namespace

      stop     : stopping the topology
                 in  detail that means
                 - shutdown docker instances with configured prefix
                 - delete all veth interfaces with configured prefix
       
      verify   : used by developer to check if script is doing fine
 
      vethclean: remove all veth instances with matching prefix from default namespace

      crpdclean: shutdown all crpd instances with user-defined prefix

USAGE
exit 0
;;
esac

