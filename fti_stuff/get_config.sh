#!/bin/bash

array=( 1 2 3 4 5 6 7 8 9 10)
f1="ip_tunnel_full_mesh.sh"
f2="ip_tunnel_flood_reflector.sh"
file=juniper.conf.gz

for i in "${array[@]}"
do
	cp /var/lib/docker/volumes/cg_fti${i}_config/_data/$file ${i}_$file
done

tar -cvvzf fti_conf.tgz *gz

