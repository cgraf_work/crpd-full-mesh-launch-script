#!/bin/bash

array=(3 4 5 6 9)
f1="ip_tunnel_full_mesh.sh"
f2="ip_tunnel_flood_reflector.sh"
f3="ip_tunnel_full_mesh_with_fti_name.sh"

for i in "${array[@]}"
do
	cp $f1 $f2 $f3 /var/lib/docker/volumes/cg_fti${i}_config/_data/
done


