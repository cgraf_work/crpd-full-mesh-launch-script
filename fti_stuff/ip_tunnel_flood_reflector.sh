#!/bin/bash

crpd=cg_fti
host=$(hostname)

function gre {
local=$1
remote=$2
address=$3
interface=$4

ip link del $interface 
ip tunnel add $interface  mode gre local $local remote $remote ttl 255
ip addr add $address dev $interface
ip link set $interface up mtu 1600 

}

#modprobe ip_gre

case $host in 
${crpd}3)
echo "setting up instance 3"

ip link delete fti0.34
ip link delete fti0.35
ip link delete fti0.36
gre 172.16.53.3 172.16.53.9 192.168.39.1/30 fti0.39
#cli -c "configure;load override /config/cg_fr3.cfg;commit and-quit" 
;;

${crpd}4)
echo "setting up instance 4"
ip link delete fti0.34
ip link delete fti0.45
ip link delete fti0.46
gre 172.16.53.4 172.16.53.9 192.168.49.1/30 fti0.49
#cli -c "configure;load override /config/cg_fr4.cfg;commit and-quit" 
;;

${crpd}5)
echo "setting up instance 5"
ip link delete fti0.35
ip link delete fti0.45
ip link delete fti0.56
gre 172.16.53.5 172.16.53.9 192.168.59.1/30 fti0.59
#cli -c "configure;load override /config/cg_fr5.cfg;commit and-quit" 
;;

${crpd}6)
echo "setting up instance 6"
ip link delete fti0.36
ip link delete fti0.46
ip link delete fti0.56
gre 172.16.53.6 172.16.53.9 192.168.69.1/30 fti0.69
i#cli -c "configure;load override /config/cg_fr6.cfg;commit and-quit" 
;;

${crpd}9)
echo "setting up instance 9"
gre 172.16.53.9 172.16.53.3 192.168.39.2/30 fti0.39
gre 172.16.53.9 172.16.53.4 192.168.49.2/30 fti0.49
gre 172.16.53.9 172.16.53.5 192.168.59.2/30 fti0.59
gre 172.16.53.9 172.16.53.6 192.168.69.2/30 fti0.69
#cli -c "configure;load override /config/cg_fr9.cfg;commit and-quit" 
;;

*)
echo "fail"
;;
esac

ip a show | grep -E "fti0.[3-9]"

