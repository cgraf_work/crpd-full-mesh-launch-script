# crpd full mesh launch script

# bash script to launch arbitrary amount of fully meshed crpd instances 

Junipers cRPD is great for testing and production purposes.

cRPD is supporting lots of different  protocols, kernel-vrf, RIFT and upcoming mpls features positions it nicely for prototyping, learning and production scenarios.

this repo only focuses on testing-environemtns to build a flexible topology by creating a fully-meshed set of crpd instances.

as of now, any parms need to be set in the script manually.

#Usage

```
root@ubuntu-cg:/mnt/sda4/christian/crpd-full-mesh-launch-script# ./crpd_full_mesh.sh
usage ./crpd_full_mesh.sh start|stop|verify|vethclean|crpdclean
       .
       start   : launching topology.
                in detail that means:
                 - if containers with same prefix exist, stop them
                 - if not existing, create veth interfaces
                 - start crpd instances
                 - move interfaces into crpd namespaces
                 - assign lo and link addresses
                 - if from older tests veth interfaces with configured prefix exist, delete those from defaut namespace

      stop     : stopping the topology
                 in  detail that means
                 - shutdown docker instances with configured prefix
                 - delete all veth interfaces with configured prefix
       
      verify   : used by developer to check if script is doing fine
 
      vethclean: remove all veth instances with matching prefix from default namespace

      crpdclean: shutdown all crpd instances with user-defined prefix
```


## variables to adopt inside the script

```
# make your changes here
########################
prefix_crpd=cg_fm                      # prefix_name of the cRPD container
oob_bridge=${prefix_crpd}
oob_cidr=172.16.51                      # e.g. 172.16.50. omit cidr
masq_ip=172.30.80.185                   # set to empty string "" or to the ip-address to masquerade
#masq_ip=""                             # set to empty string "" or to the ip-address to masquerade
base_port=9900                          # when masquerade then crpd is base_port + id
prefix_link=${prefix_crpd}              # prefix of the links to create
                                        # must be shorter or equa then 15chars in total
                                        # the script adds 2 * underscores and 2 * number
                                        # with e.g. 15 instances the longest name would be
                                        # cgv_15_14
                                        # so in most cases add 6 chars on top of prefix 
crpd_image="crpd:20.2R1.10"             # name of crpd image. no need to have it equal to the container-name
instances=5                             # number of crpd instances to create

# do not change below
#####################
link_sub="10"                           # its a lab. easy to remember numbering is important
                                        # hence the used numbering is wasting 10/8
                                        # link node 30 to node 41 uses this subnet: 10.30.41/24

```

#launching script

```
# ./crpd_full_mesh.sh start
...
...
crpd_instance cg_xfm1 uses oob 172.16.51.1 and externally exposed via ip:port 172.30.80.185:9901                           
crpd_instance cg_xfm2 uses oob 172.16.51.2 and externally exposed via ip:port 172.30.80.185:9902                           
crpd_instance cg_xfm3 uses oob 172.16.51.3 and externally exposed via ip:port 172.30.80.185:9903                           
crpd_instance cg_xfm4 uses oob 172.16.51.4 and externally exposed via ip:port 172.30.80.185:9904
crpd_instance cg_xfm5 uses oob 172.16.51.5 and externally exposed via ip:port 172.30.80.185:9905
...


examples to connect to first running instance
========
docker shell using "docker exec"
    docker exec -ti cg_xfm1 cli
    docker exec -ti cg_xfm1 bash
     
enabling ssh
    you might want to enable ssh following the manual: 
    https://www.juniper.net/documentation/en_US/crpd/topics/topic-map/establishing-ssh-crpd.html
     
ssh from local host
    ssh -l root 172.16.51.1

ssh from a remote host (works only if variable masq_ip is set to your externally reachable interface)
    ssh -p 9901 -l root 172.30.80.185

link naming and addressing
=========================
configured links
    all instances are fully meshed configured
    e.g. on the 3rd instance cg_xfm3 the link named to connect to instance 14 is: cg_xfm_3_14
    e.g. on the 14th instance cg_xfm14 the link named to connect to instance 3 is: cg_xfm_14_3

ip-addresses on the links
    instance cg_xfm3 link cg_xfm_3_14 has ip 10.3.14.1/24
    instance cg_xfm14 link cg_xfm_14_3 has ip 10.3.14.2/24


```


have fun


thanks

christian




