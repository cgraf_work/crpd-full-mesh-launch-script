#!/bin/bash

# $1 = name of docker container
# $2 = interface to add. could be even a vlan-interface


c1=$1
interface=$2

# check if interface got provided
if [ -z "$interface" ]; then
	    echo "$0 <container> <interface>"
	        exit 1
fi

set -e  # terminate on error

sudo mkdir -p /var/run/netns

function check_container_up {
SECONDS=0

until [ ! -z "$(docker ps -q -f name=$c1)" ]; do
   echo "waiting for container $c1 ..."
   sleep 1
   if [ $SECONDS -gt 15 ]; then
	echo "$c1 not running"
        exit 1
   fi
   ((SECONDS=SECONDS+1))
done
}

function get_pid {
echo "container provided: $c1"
#fc1=$(docker ps -q -f name=$c1)
#echo "$c1 $fc1"
#pid1=$(docker inspect -f "{{.State.Pid}}" $fc1)
pid1=$(docker inspect -f "{{.State.Pid}}" $c1)
echo "debug - pid = $pid1"
if [ -z "$pid1" ]; then
	echo "Can't find pid for container $c1"
	exit 1
fi
echo "$c1 has pid $pid1"
}

function check_interface_avail {
# wait for the interface
SECONDS=0
echo "checking interface $interface if existing"
until [[ $(ip link show $interface) ]]; do
   echo "waiting for interface $interface ..."
   sleep 1
   if [ $SECONDS -gt 15 ]; then
	echo "$interface not found"
        exit 1
   fi
   ((SECONDS=SECONDS+1))
done
echo "link $interface is existing"
}


# main

echo .
echo "executing script $0" 
# wait for the interface
check_container_up 
get_pid
echo "linking namespace of container $c1 with pid $pid1 to /var/run"
sudo ln -sf /proc/$pid1/ns/net /var/run/netns/$c1

#check_interface_avail
#sudo ifconfig $interface mtu 3000 || echo "cant adjust mtu on $interface, ignoring"
#sudo ethtool -L $interface combined 2
				 
echo "moving endpoints to netns ..."
check_interface_avail
sudo ip link set $interface netns $c1

# waiting that interface arrived in namespace
#until ip netns exec $c1 ip link show $interface
#do
#sleep 1
#done

#echo "bringing links up ..."
#sudo ip netns exec $c1 ip link set up $interface

#until ip address show $interface | grep -Eq ": $interface:.*state UP" 
#do
#	sleep 1
#done
echo "finished script $0" 
echo .
