#!/bin/bash

crpd=cg_fti
host=$(hostname)

function gre {
local=$1
remote=$2
address=$3
interface=$4

ip link del $interface 
ip tunnel add $interface  mode gre local $local remote $remote ttl 255
ip addr add $address dev $interface
ip link set $interface up mtu 1600 

}

#modprobe ip_gre


case $host in 
${crpd}3)
echo "setting up instance 3"
ip link delete fti39
gre 10.3.4.1 10.3.4.2 192.168.34.1/30 gre34
gre 10.3.5.1 10.3.5.2 192.168.35.1/30 gre35
gre 10.3.6.1 10.3.6.2 192.168.36.1/30 gre36
cli -c "configure;load override /config/cg_fm3.cfg;commit and-quit" 
;;

${crpd}4)
echo "setting up instance 4"
ip link delete fti49
gre 10.3.4.2 10.3.4.1 192.168.34.2/30 gre43
gre 10.4.5.1 10.4.5.2 192.168.45.1/30 gre45
gre 10.4.6.1 10.4.6.2 192.168.46.1/30 gre46
cli -c "configure;load override /config/cg_fm4.cfg;commit and-quit" 
;;

${crpd}5)
echo "setting up instance 5"
ip link delete fti59
gre 10.3.5.2 10.3.5.1 192.168.35.2/30 gre53
gre 10.4.5.2 10.4.5.1 192.168.45.2/30 gre54
gre 10.5.6.1 10.5.6.2 192.168.56.1/30 gre56
cli -c "configure;load override /config/cg_fm5.cfg;commit and-quit" 
;;

${crpd}6)
echo "setting up instance 6"
ip link delete fti69
gre 10.3.6.2 10.3.6.1 192.168.36.2/30 gre63
gre 10.4.6.2 10.4.6.1 192.168.46.2/30 gre64
gre 10.5.6.2 10.5.6.1 192.168.56.2/30 gre65
cli -c "configure;load override /config/cg_fm6.cfg;commit and-quit" 
;;

${crpd}9)
echo "setting up instance 9"
ip link delete fti0.93
ip link delete fti0.94
ip link delete fti0.95
ip link delete fti0.96
cli -c "configure;load override /config/cg_fm9.cfg;commit and-quit" 
;;

*)
echo "fail"
;;
esac

ip a show | grep gre

