#!/bin/bash

array=(3 4 5 6 9)
f1="ip_tunnel_full_mesh.sh"
f2="ip_tunnel_flood_reflector.sh"

for i in "${array[@]}"
do
	cp $f1 $f2 /var/lib/docker/volumes/cg_fti${i}_config/_data/
done


