#!/bin/bash


SECONDS=0
interface=$1
until [[ $(ifconfig $interface) ]]; do
echo "waiting for interface $interface ..."
sleep 1
if [ $SECONDS -gt 15 ]; then
echo "$interface not found"
exit 1
fi
((SECONDS=SECONDS+1))
done

